# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 13:37:50 2021

@author: fossi
"""

import pandas as pd
import numpy as np


"""
Part 1
"""
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data',header=None)
df.columns = ['sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'types']

indeks = [f'flower {k+1}' for k in range(len(df.index))]
df.index = indeks

"""
Part 2
"""

unique_list = list(df.types.unique())

df_bytypes = df.groupby(df['types'])
mean = df_bytypes.mean()
print(mean)


df['sepal width >= 3'] = np.where(df['sepalwidth'] >= 3, True, False)

true_count = df['sepal width >= 3'].sum()
print(true_count)


"""
Part 3
"""
df_setosa = df[df['types'] == 'Iris-setosa']
df_versicolor = df[df['types'] == 'Iris-versicolor']
df_verginica = df[df['types'] == 'Iris-virginica']

ClassCount = df['types'].value_counts()
print(ClassCount)

#print last 10 rows of coloumns sepal length and types
print(df[['sepallength','types']].tail(10))

#Print rows where sepal length > 5 and petal width > 0.2
df.loc[(df['sepallength'] > 5) & (df['petalwidth'] > 0.2)]

petalwidth_18 = df.loc[df['petalwidth'] == 1.8]
petalwidth_18
#Get statistics
print(df.describe())
print(df['petallength'].describe())

#Delete rows and coloumns 
df = df.drop(['flower 55','flower 77'], axis = 0)
df = df.drop('sepal width >= 3', axis = 1)
print(df.head())
#view all rows of sepal length where petal width is exactly 1.8
print(df['sepallength'].loc[df['petalwidth'] == 1.8])

#get values of the dataframe stored in a numpy array
data = df.values

# remove types and apply a funcition named computation
df = df.drop('types',axis = 1)

computation = lambda x: (x + 1)*3
df_new = df.applymap(computation)






